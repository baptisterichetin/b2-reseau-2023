# I. Faire joujou avec l'asynchrone

## 1. Premiers pas

### 🌞 sleep_and_print.py

[sleep_and_print.py](/tp6/py/sleep_and_print.py)

### 🌞 sleep_and_print_async.py

[sleep_and_print_async.py](/tp6/py/sleep_and_print_async.py)

## 2. Web Requests

### 🌞 web_sync.py

[web_sync.py](/tp6/py/web_sync.py)

### 🌞 web_async.py

[web_async.py](/tp6/py/web_async.py)

### 🌞 web_sync_multiple.py

[web_sync_multiple.py](/tp6/py/web_sync_multiple.py)

### 🌞 web_async_multiple.py

[web_async_multiple.py](/tp6/py/web_async_multiple.py)



### 🌞 Mesure !

Version sync
```
PS C:\Users\bapti\Desktop\b2-reseau-2023\tp6\py> python .\web_sync_multiple.py urls.txt
Processing URL: https://www.twitch.tv/
Web content downloaded and saved to: tmp/web_www.twitch.tv_.html
Processing URL: https://www.youtube.com/
Web content downloaded and saved to: tmp/web_www.youtube.com_.html
Total execution time: 1.5419254302978516 seconds
```

Version async
```
PS C:\Users\bapti\Desktop\b2-reseau-2023\tp6\py> python .\web_async_multiple.py urls.txt
Web content downloaded and saved to: tmp/web_www.twitch.tv_.html
Web content downloaded and saved to: tmp/web_www.youtube.com_.html
Total execution time: 0.30277252197265625 seconds
```
import requests
import sys
import os
import time

def get_content(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.text
    except requests.exceptions.RequestException as e:
        return f"Error: {e}"

def write_content(content, url):
    file_name = f"tmp/web_{url.replace('https://', '').replace('/', '_')}.html"
    
    with open(file_name, "w", encoding="utf-8") as f:
        f.write(content)

    print(f"Web content downloaded and saved to: {file_name}")

def process_urls(file_path):
    start_time = time.time()

    with open(file_path, "r") as file:
        urls = file.readlines()

    for url in urls:
        url = url.strip()
        print(f"Processing URL: {url}")
        content = get_content(url)
        write_content(content, url)

    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f"Total execution time: {elapsed_time} seconds")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python web_sync_multiple.py <file_path>")
        sys.exit(1)

    file_path = sys.argv[1]
    process_urls(file_path)

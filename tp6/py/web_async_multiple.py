import aiohttp
import asyncio
import sys
import os
import time
import aiofiles

async def get_content_async(url, session):
    async with session.get(url) as response:
        return await response.text()

async def write_content_async(content, url):
    file_name = f"tmp/web_{url.replace('https://', '').replace('/', '_')}.html"
    async with aiofiles.open(file_name, 'w', encoding='utf-8') as f:
        await f.write(content)
    print(f"Web content downloaded and saved to: {file_name}")

async def process_url_async(url, session):
    content = await get_content_async(url, session)
    await write_content_async(content, url)

async def process_urls_async(file_path):
    start_time = time.time()

    async with aiohttp.ClientSession() as session:
        with open(file_path, "r") as file:
            urls = file.readlines()

        tasks = [process_url_async(url.strip(), session) for url in urls]
        await asyncio.gather(*tasks)

    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f"Total execution time: {elapsed_time} seconds")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python web_async_multiple.py <file_path>")
        sys.exit(1)

    file_path = sys.argv[1]
    asyncio.run(process_urls_async(file_path))

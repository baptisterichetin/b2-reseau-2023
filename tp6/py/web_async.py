import aiohttp
import aiofiles
import asyncio
import sys
import os

async def get_content_async(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            content = await response.text()
    return content

async def write_content_async(content, file):
    working_directory = os.getcwd()
    async with aiofiles.open(working_directory + file, 'w', encoding='utf-8') as f:
        await f.write(content)

async def main():
    url = sys.argv[1]
    content = await get_content_async(url)

    file = "/tmp/web_page.html"
    await write_content_async(content, file)

    print(f"Web content downloaded and saved to: {file}")

if __name__ == "__main__":
    asyncio.run(main())

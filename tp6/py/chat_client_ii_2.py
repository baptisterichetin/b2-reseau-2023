

import socket

def main():
    # Server information
    server_address = ('10.1.1.10', 13337)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(server_address)

        # Send a greeting to the server
        s.sendall(b"Hello")

        # Receive and print the server's welcome message
        data = s.recv(1024)
        
        print("Received from server:", data.decode())

if __name__ == "__main__":
    main()

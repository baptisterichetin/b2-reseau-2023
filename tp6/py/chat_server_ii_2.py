import asyncio

async def handle_client(reader, writer):
    # Get client information
    client_address = writer.get_extra_info('peername')
    print(f"Received connection from {client_address}")

    # Send a welcome message to the client
    welcome_message = f"Hello {client_address[0]}:{client_address[1]}"
    writer.write(welcome_message.encode())
    await writer.drain()

    # Receive and print the client's message
    data = await reader.read(100)
    message = data.decode()
    print(f"Received message from {client_address}: {message}")

 
    confirmation_message = "Message received: Hello"
    writer.write(confirmation_message.encode())
    await writer.drain()

    # Close the connection
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle_client, '10.1.1.10', 13337)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())

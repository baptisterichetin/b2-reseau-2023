import requests
import sys
import os

def get_content(url):
    r = requests.get(url)
    content = r.text
    return content

def write_content(content, file):
    working_directory = os.getcwd()
    f=open(working_directory+file,"w", encoding="utf-8")
    f.write(content)
    f.close()

if __name__ == "__main__":

    url = sys.argv[1]
    content = get_content(url)

    file = "/tmp/web_page.html"    
    write_content(content, file)

    print(f"Web content downloaded and saved to: {file}")

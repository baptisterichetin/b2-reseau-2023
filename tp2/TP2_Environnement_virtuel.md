# TP2 : Environnement virtuel

## I. Topologie réseau

### ☀️ Sur node1.lan1.tp2

- Afficher ses cartes réseau
```
[baptiste@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3c:1b:6f brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.11/24 brd 10.1.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3c:1b6f/64 scope link
       valid_lft forever preferred_lft forever
```

- afficher sa table de routage
```
[baptiste@node1 ~]$ ip route show
10.1.1.0/24 dev enp0s3 proto kernel scope link src 10.1.1.11 metric 100
10.1.2.0/24 via 10.1.1.254 dev enp0s3
```

- prouvez qu'il peut joindre node2.lan2.tp2
```
[baptiste@node1 ~]$ ping 10.1.2.12
PING 10.1.2.12 (10.1.2.12) 56(84) bytes of data.
64 bytes from 10.1.2.12: icmp_seq=1 ttl=63 time=0.400 ms
64 bytes from 10.1.2.12: icmp_seq=2 ttl=63 time=0.508 ms
64 bytes from 10.1.2.12: icmp_seq=3 ttl=63 time=0.629 ms
64 bytes from 10.1.2.12: icmp_seq=4 ttl=63 time=0.661 ms
64 bytes from 10.1.2.12: icmp_seq=5 ttl=63 time=0.515 ms
64 bytes from 10.1.2.12: icmp_seq=6 ttl=63 time=0.625 ms
^C
--- 10.1.2.12 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5133ms
rtt min/avg/max/mdev = 0.400/0.556/0.661/0.090 ms
```

- prouvez avec un traceroute que le paquet passe bien par router.tp2
```
[baptiste@node1 ~]$ traceroute 10.1.2.11
traceroute to 10.1.2.11 (10.1.2.11), 30 hops max, 60 byte packets
 1  10.1.1.254 (10.1.1.254)  0.871 ms  0.868 ms  0.846 ms
 2  10.1.2.11 (10.1.2.11)  0.772 ms !X  0.711 ms !X  0.620 ms !X
```

---

## II. Interlude accès internet

### ☀️ Sur router.tp2

```
[baptiste@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=56 time=15.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=56 time=13.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=56 time=16.0 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=56 time=13.9 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 13.472/14.694/15.959/1.034 ms
```

```
[baptiste@router ~]$ ping www.google.com
PING www.google.com (142.250.201.164) 56(84) bytes of data.
64 bytes from par21s23-in-f4.1e100.net (142.250.201.164): icmp_seq=1 ttl=56 time=26.5 ms
64 bytes from par21s23-in-f4.1e100.net (142.250.201.164): icmp_seq=2 ttl=56 time=25.2 ms
64 bytes from par21s23-in-f4.1e100.net (142.250.201.164): icmp_seq=3 ttl=56 time=26.2 ms
^C
--- www.google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 25.168/25.940/26.488/0.561 ms
```

### ☀️ Accès internet LAN1 et LAN2

```
[baptiste@node2.lan1.tp2 ~]$ sudo ip route add default via 10.1.1.254
[sudo] password for baptiste:

[baptiste@node2.lan1.tp2 ~]$ ip route show
default via 10.1.1.254 dev enp0s3
10.1.1.0/24 dev enp0s3 proto kernel scope link src 10.1.1.12 metric 100
10.1.2.0/24 via 10.1.1.254 dev enp0s3


[baptiste@node2.lan2.tp2 ~]$ sudo ip route add default via 10.1.2.254
[sudo] password for baptiste:

[baptiste@node2.lan2.tp2 ~]$ ip route show
default via 10.1.2.254 dev enp0s3
10.1.1.0/24 dev enp0s3 proto kernel scope link src 10.1.1.12 metric 100
10.1.2.0/24 via 10.1.2.254 dev enp0s3
```

```
[baptiste@node2 ~]$ sudo echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf
nameserver 8.8.8.8
```

```
[baptiste@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=55 time=17.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=55 time=15.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=55 time=18.1 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 15.230/17.009/18.076/1.266 ms


[baptiste@node2 ~]$ ping google.com
PING google.com (142.250.201.46) 56(84) bytes of data.
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=1 ttl=114 time=16.9 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=2 ttl=114 time=18.9 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=3 ttl=114 time=17.6 ms
64 bytes from mrs08s20-in-f14.1e100.net (142.250.201.46): icmp_seq=4 ttl=114 time=15.8 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 15.792/17.292/18.894/1.121 ms
```

---

## III. Services réseau

### 1. DHCP

### ☀️ Sur dhcp.lan1.tp2

```
[baptiste@dhcp ~]$ hostname
dhcp.lan1.tp2
```

```
[baptiste@dhcp ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:18:8e:25 brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.253/24 brd 10.1.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe18:8e25/64 scope link
       valid_lft forever preferred_lft forever
```

```
[baptiste@dhcp ~]$ sudo dnf install dhcp
```

```
[baptiste@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
subnet 10.1.1.0 netmask 255.255.255.0 {
    range 10.1.1.100 10.1.1.200;
    option routers 10.1.1.254;
    option domain-name-servers 1.1.1.1;
}


[baptiste@dhcp ~]$ sudo systemctl restart dhcpd


[baptiste@dhcp ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; disabled; preset: disabled)
     Active: active (running) since Mon 2023-10-23 21:29:44 CEST; 8s ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 45054 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 5905)
     Memory: 5.2M
        CPU: 7ms
     CGroup: /system.slice/dhcpd.service
             └─45054 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid
```

### ☀️ Sur node1.lan1.tp2

```
[baptiste@node1 ~]$ sudo dnf -y install dhcp-client
[sudo] password for baptiste:
Last metadata expiration check: 0:15:08 ago on Mon 23 Oct 2023 09:33:53 PM CEST.
Package dhcp-client-12:4.4.2-18.b1.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[baptiste@node1 ~]$ sudo nmcli connection modify enp0s3 ipv4.method auto
[baptiste@node1 ~]$ sudo nmcli connection down enp0s3; nmcli connection up enp0s3

[baptiste@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3c:1b:6f brd ff:ff:ff:ff:ff:ff
    inet 10.1.1.100/24 brd 10.1.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 43109sec preferred_lft 43109sec
    inet6 fe80::a00:27ff:fe3c:1b6f/64 scope link noprefixroute
       valid_lft forever preferred_lft forever


[baptiste@node1 ~]$ ip route
default via 10.1.1.254 dev enp0s3 proto dhcp src 10.1.1.100 metric 100
10.1.1.0/24 dev enp0s3 proto kernel scope link src 10.1.1.100 metric 100
10.1.2.0/24 via 10.1.1.254 dev enp0s3


[baptiste@node1 ~]$ ping 10.1.2.11
PING 10.1.2.11 (10.1.2.11) 56(84) bytes of data.
64 bytes from 10.1.2.11: icmp_seq=1 ttl=63 time=0.453 ms
64 bytes from 10.1.2.11: icmp_seq=2 ttl=63 time=0.526 ms
64 bytes from 10.1.2.11: icmp_seq=3 ttl=63 time=0.384 ms
64 bytes from 10.1.2.11: icmp_seq=4 ttl=63 time=0.588 ms
^C
--- 10.1.2.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3085ms
rtt min/avg/max/mdev = 0.384/0.487/0.588/0.076 ms
```

## 2. Web web web

### ☀️ Sur web.lan2.tp2

```
[baptiste@web ~]$ hostname
web.lan2.tp2
 ```

 ```
 [baptiste@web ~]$ sudo dnf install nginx

[baptiste@web ~]$ sudo mkdir -p /var/www/site_nul
echo "Ceci est un site web tout nul." | sudo tee /var/www/site_nul/index.html
Ceci est un site web tout nul.
 ```

```
[baptiste@web ~]$ sudo cat /etc/nginx/conf.d/site_nul.conf
server {
    listen 80;
    server_name site_nul.tp2;
    root /var/www/site_nul;
}

[baptiste@web ~]$ sudo systemctl restart nginx

[baptiste@web ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-10-23 22:04:24 CEST; 4min 5s ago
    Process: 10953 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 10954 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 10955 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 10956 (nginx)
      Tasks: 2 (limit: 5905)
     Memory: 2.0M
        CPU: 15ms
     CGroup: /system.slice/nginx.service
             ├─10956 "nginx: master process /usr/sbin/nginx"
             └─10957 "nginx: worker process"
```

```
[baptiste@web ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
success
[baptiste@web ~]$ sudo firewall-cmd --reload
success

[baptiste@web ~]$ ss -tuln | grep 80
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*
tcp   LISTEN 0      511             [::]:80           [::]:*
```

```
[baptiste@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

### ☀️ Sur node1.lan1.tp2

```
[baptiste@node1 ~]$ sudo cat /etc/hosts
 10.1.2.12 site_nul.tp2


 [baptiste@node1 ~]$ curl site_nul.tp2
Ceci est un site web tout nul.
```


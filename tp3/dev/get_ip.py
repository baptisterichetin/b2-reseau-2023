from psutil import net_if_addrs

def get_wireless_ip(wireless_interface_name):
    for interface in net_if_addrs():
        if interface == wireless_interface_name:
            for address in net_if_addrs()[interface]:
                if address.family == 2:
                    return address.address
    return None

wireless_interface_name = 'Wifi Ynov'

if __name__ == "__main__":
    ip = get_wireless_ip(wireless_interface_name)
    if ip:
        print(f"Adresse IP de l'interface {wireless_interface_name}: {ip}")
    else:
        print(f"Aucune adresse IP IPv4 trouvée pour l'interface {wireless_interface_name}.")
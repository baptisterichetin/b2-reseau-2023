import os
import sys

ping_command = f"ping {sys.argv[1]}"

ping_output = os.popen(ping_command).read()

if "TTL=" in ping_output:
  print(" Is up!")
else:
  print("Is down!")
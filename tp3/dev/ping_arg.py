import os
from sys import argv

def myping(host):
    response = os.system("ping " + host)

    if response == 0:
        return "IS UP!"
    else:
        return "IS DOWN!"

print(myping(argv[1]))
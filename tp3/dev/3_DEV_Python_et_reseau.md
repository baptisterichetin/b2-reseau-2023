# TP3 DEV : Python et réseau

## I. Ping

### 🌞 Ping_simple.py

[Mon code pour Ping](./ping_simple.py)



### 🌞 Ping_arg.py

[Mon code pour Argv](./ping_arg.py)


### 🌞 Is_up.py

[Mon code pour UP ou Down](./is_up.py)




## II. DNS

### 🌞 Lookup.py

[Utiliser gethostbyname()](./lookup.py)



## III. Get your IP

### 🌞 Get_ip.py

[ Afficher l'adresse IP de ma carte WiFi](./get_ip.py)



## IV. Mix

### 🌞 Network.py

[Mon code network.py](./network.py)

## V. Deploy


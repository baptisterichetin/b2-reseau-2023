from psutil import net_if_addrs
from socket import gethostbyname
from sys import argv
import os
import sys


def lookup(name):
    try:
        ip = gethostbyname(name)
        print("The IP address of", name, "is", ip)
    except:
        print("The hostname", name, "could not be resolved.")
        exit(1)

def ping(ip):
    ping_command = f"ping {ip}"
    ping_output = os.popen(ping_command).read()
    if "TTL=" in ping_output:
        print(" Is up!")
    else:
        print("Is down!")


def ip():
    try:
        wireless_interface_name = 'Wifi Ynov'
        myip = get_wireless_ip(wireless_interface_name)
        if myip:
            print(myip)
        else:
            print(f"No IPv4 address found for interface {wireless_interface_name}.")
    except Exception as e:
        print(e)

def get_wireless_ip(wireless_interface_name):
    for interface in net_if_addrs():
        if interface == wireless_interface_name:
            for address in net_if_addrs()[interface]:
                if address.family == 2:
                    return address.address
    return None


if argv[1] == "lookup":
    lookup(argv[2])

elif argv[1] == "ping":
    ping(argv[2])

elif argv[1] == "ip":
    ip()

else:
    print(f"{argv[1]} is not an available command. Déso."  )
    exit(1)
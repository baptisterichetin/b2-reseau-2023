# b2-reseau-2023

## Rendu du [TP1](/tp1/)
## Rendu du [TP2](/tp2/)
## Rendu du [TP3](/tp3/dev/)
## Rendu du [TP4](/tp4/)
- [I._Simple_bs_program](./tp4/I._Simple_bs_program.md)
- [II._You_say_dev_I_say_good_practices](./tp4/II._You_say_dev_I_say_good_practices.md)
- [III._COMPUTE](./tp4/III._COMPUTE.md)
## Rendu du [TP5](/tp5/)
- [II. Opti calculatrice](/tp5/II._Opti_calculatrice.md)
- [III. Serveur Web HTTP](/tp5/III._Serveur_Web_et_HTTP.md)

## Rendu du [TP6](/tp6/)
- [I. Débuts avec l'asynchrone](/tp6/I_Débuts_avec_l'asynchrone.md)
- [II. Chat Room](/tp6/II._Chat_Room.md)
- [III. Bonus](/tp6/III._Bonus.md)


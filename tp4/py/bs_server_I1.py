import socket

# Définition de l'adresse IP et du port
ip_address = '10.1.1.10'  # Remplace cela par l'adresse IP de ton serveur
port = 13337

# Création d'une socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Liaison de la socket à l'adresse IP et au port spécifiés
server_socket.bind((ip_address, port))

# Écoute de la socket
server_socket.listen(5)
print(f"Le serveur écoute sur {ip_address}:{port}")

while True:
    # Attente de la connexion d'un client
    client_socket, client_address = server_socket.accept()
    print(f"Connexion établie avec {client_address}")

    # Envoi du message au client
    message = "Hi mate !"
    client_socket.send(message.encode())

    # Réception de la réponse du client
    response = client_socket.recv(1024).decode()
    print(f"Réponse du client : {response}")

    # Fermeture de la connexion avec le client
    client_socket.close()

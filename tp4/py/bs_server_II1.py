import socket
import argparse
import sys

def start_server(args):
    # Création d'une socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Obtenir l'adresse IP de la machine
        ip_address = socket.gethostbyname(socket.gethostname())

        # Liaison de la socket à l'adresse IP et au port spécifiés
        server_socket.bind((ip_address, args.port))

        # Écoute de la socket
        server_socket.listen(5)
        print(f"Le serveur écoute sur {ip_address}:{args.port}")

        while True:
            # Attente de la connexion d'un client
            client_socket, client_address = server_socket.accept()
            print(f"Un client vient de se connecter et son IP c'est {client_address[0]}.")

            # Réception du message du client
            message = client_socket.recv(1024).decode()
            print(f"Message du client : {message}")

            # Traitement de la réponse en fonction du message du client
            if "meo" in message.lower():
                response = "Meo à toi confrère."
            elif "waf" in message.lower():
                response = "ptdr t ki"
            else:
                response = "Mes respects humble humain."

            # Envoi de la réponse au client
            client_socket.send(response.encode())

            # Fermeture de la connexion avec le client
            client_socket.close()

    finally:
        # Fermeture propre de la connexion avec le serveur
        server_socket.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Serveur TCP avec gestion d\'options.')

    parser.add_argument('-p', '--port', type=int, default=13337, help='Numéro de port pour écouter les connexions. (Par défaut : 13337)')

    args = parser.parse_args()
    start_server(args)

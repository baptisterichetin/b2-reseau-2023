import re
import logging
from logging.handlers import RotatingFileHandler
import socket
import select

def setup_logger():
    # Création du logger
    logger = logging.getLogger('bs_client')
    logger.setLevel(logging.DEBUG)  # Affiche tous les logs

    # Création d'un formateur pour les logs
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # Création d'un gestionnaire de logs pour la console (niveau INFO)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    # Création d'un gestionnaire de logs pour le fichier (niveau DEBUG)
    log_file_path = '/var/log/bs_client/bs_client.log'
    file_handler = RotatingFileHandler(log_file_path, maxBytes=10240, backupCount=1)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

try:
    # Configuration du logger
    logger = setup_logger()

    # Saisie de l'opération arithmétique
    operation = input("Saisis une opération arithmétique (addition, soustraction, multiplication) : ")

    # Vérification avec une expression régulière
    pattern = re.compile(r'^(-?\d+)\s*([-+*/])\s*(-?\d+)$')
    match = pattern.match(operation)

    if not match:
        raise ValueError("Opération invalide. Utilisez seulement des opérations arithmétiques avec des nombres entiers.")

    # Envoi de l'opération au serveur
    server_ip = '10.1.1.10'  # Remplace cela par l'adresse IP de ton serveur
    server_port = 13337

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((server_ip, server_port))
    logger.info(f"Connecté au serveur {server_ip}:{server_port}")

    client_socket.send(operation.encode())

    # Attente de la réponse du serveur avec un timeout de 5 secondes
    ready, _, _ = select.select([client_socket], [], [], 5)
    if ready:
        response = client_socket.recv(1024).decode()
        logger.info(f"Réponse du serveur : {response}")
    else:
        raise TimeoutError("Timeout : aucune réponse du serveur dans le délai imparti.")

except ValueError as ve:
    logger.error(f"Erreur de valeur : {ve}")
except TimeoutError as te:
    logger.error(f"Timeout : {te}")
except Exception as e:
    logger.error(f"Erreur inattendue : {e}")
finally:
    # Fermeture propre de la connexion avec le serveur
    client_socket.close()

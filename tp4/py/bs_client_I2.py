import socket
import sys

server_ip = '10.1.1.10'  
server_port = 13337

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    client_socket.connect((server_ip, server_port))
    print(f"Connecté avec succès au serveur {server_ip} sur le port {server_port}")

    message = input("Que veux-tu envoyer au serveur : ")
    client_socket.send(message.encode())

    response = client_socket.recv(1024).decode()
    print(f"Réponse du serveur : {response}")

except Exception as e:
    print(f"Erreur : {e}")
    sys.exit(1)

finally:

    client_socket.close()
    sys.exit(0)

import socket
import argparse
import logging
from logging.handlers import TimedRotatingFileHandler
import re
from datetime import datetime, timedelta

def setup_logger():
    # Création du logger
    logger = logging.getLogger('bs_server')
    logger.setLevel(logging.DEBUG)

    # Création d'un formateur pour les logs
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # Création d'un gestionnaire de logs pour la console (niveau INFO)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    # Création d'un gestionnaire de logs pour le fichier (niveau DEBUG)
    file_handler = TimedRotatingFileHandler('/var/log/bs_server/bs_server.log', when='midnight', interval=1, backupCount=7)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger

def start_server(port):
    # Création d'une socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    logger = setup_logger()
    last_client_time = datetime.now()
    result = None

    try:
        # Obtenir l'adresse IP de la machine
        ip_address = socket.gethostbyname(socket.gethostname())

        # Liaison de la socket à l'adresse IP et au port spécifiés
        server_socket.bind((ip_address, port))

        # Écoute de la socket
        server_socket.listen(5)
        logger.info(f"Lancement du serveur. Le serveur tourne sur {ip_address}:{port}")

        while True:
            # Attente de la connexion d'un client
            client_socket, client_address = server_socket.accept()
            logger.info(f"Connexion d'un client. Un client {client_address[0]} s'est connecté.")

            # Réception du message du client
            client_request = client_socket.recv(1024).decode()
            logger.info(f"Message reçu d'un client. Le client {client_address[0]} a envoyé : {client_request}.")

            # Contrôle de l'opération avec une expression régulière
            if not re.match(r'^\s*([-+]?\d+)\s*([+\-*/])\s*([-+]?\d+)\s*$', client_request):
                logger.error("Opération invalide. Veuillez saisir une opération arithmétique valide.")
                client_socket.send("Opération invalide. Veuillez saisir une opération arithmétique valide.".encode())
                continue

            # Interprétation de l'opération et stockage du résultat
            result = eval(client_request)
            logger.info(f"Résultat de l'opération : {result}")

            # Envoi de la réponse au client
            client_socket.send(str(result).encode())

            # Met à jour le temps du dernier client connecté
            last_client_time = datetime.now()

            # Fermeture de la connexion avec le client
            client_socket.close()

    except KeyboardInterrupt:
        pass
    finally:
        # Fermeture propre de la connexion avec le serveur
        server_socket.close()

def main():
    # Configuration de l'analyseur d'arguments
    parser = argparse.ArgumentParser(description='Serveur TCP avec gestion d\'options.')
    parser.add_argument('-p', '--port', type=int, default=13337, help='Numéro de port pour écouter les connexions. (Par défaut : 13337)')

    # Analyse des arguments de la ligne de commande
    args = parser.parse_args()

    # Traitement des options
    if args.port < 0 or args.port > 65535:
        print("ERROR Le port spécifié n'est pas un port possible (de 0 à 65535).")
        exit(1)
    elif args.port <= 1024:
        print("ERROR Le port spécifié est un port privilégié. Spécifiez un port au-dessus de 1024.")
        exit(2)

    start_server(args.port)

if __name__ == "__main__":
    main()

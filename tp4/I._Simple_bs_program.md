# I. Simple bs program

## 1. First steps

### 🌞 bs_server_I1.py

[bs_server_I1.py](/tp4/py/bs_server_I1.py)

### 🌞 bs_client_I1.py

[bs_client_I1.py](/tp4/py/bs_client_I1.py)

### 🌞 Commandes...

```
[baptiste@bsserver tp4]$ sudo firewall-cmd --zone=public --add-port=13337/tcp --permanent
[sudo] password for baptiste:
success
[baptiste@bsserver tp4]$ sudo firewall-cmd --reload
success

[baptiste@bsserver ~]$ sudo ss -tnlp | grep 13337
[sudo] password for baptiste:
LISTEN 0      5          10.1.1.10:13337      0.0.0.0:*    users:(("python",pid=1410,fd=3))
```

```
[baptiste@bsserver ~]$ sudo dnf install python3 -y
[sudo] password for baptiste:
Last metadata expiration check: 0:14:11 ago on Tue 07 Nov 2023 02:16:31 PM CET.
Package python3-3.9.16-1.el9_2.2.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```
[baptiste@bsserver tp4]$ python bs_server_I1.py
Le serveur écoute sur 10.1.1.10:13337
Connexion établie avec ('10.1.1.11', 40032)
Réponse du client : Meooooo !


[baptiste@bsclient tp4]$ python bs_client_I1.py
Connecté au serveur 10.1.1.10:13337
Réponse du serveur : Hi mate !
```
---

## 2. User friendly

### 🌞 bs_client_I2.py

[bs_client_I2.py](/tp4/py/bs_client_I2.py)

### 🌞 bs_server_I2.py

[bs_server_I2.py](/tp4/py/bs_server_I2.py)

---

## 3. You say client I hear control

### 🌞 bs_client_I3.py

[bs_client_I3.py](/tp4/py/bs_client_I3.py)
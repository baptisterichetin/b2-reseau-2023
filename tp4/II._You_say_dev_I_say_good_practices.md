# II. You say dev I say good practices

## 1. Args

### 🌞 bs_server_II1.py

[bs_server_II1.py](/tp4/py/bs_server_II1.py)

## 2. Logs

### 🌞 bs_server_II2A.py

```
[baptiste@bsserver py]$ sudo mkdir -p /var/log/bs_server/
```

[bs_server_II2A.py](/tp4/py/bs_server_II2A.py)

### 🌞 bs_client_II2B.py

```
sudo mkdir -p /var/log/bs_client/
```

[bs_client_II2B.py-](/tp4/py/bs_client_II2B.py)



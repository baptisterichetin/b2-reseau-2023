# TP1 : Maîtrise réseau du poste

## I. Basics

### ☀️ **Carte réseau WiFi**


```
PS C:\Windows\system32> ipconfig /all

Carte réseau sans fil Wifi Ynov :
Adresse physique . . . . . . . . . . . : 8C-B8-7E-9A-B8-5C
Adresse IPv4. . . . . . . . . . . . . .: 10.33.76.170(préféré)
- en notation CIDR : Masque de sous-réseau. . . . . . . . . : /20
- et en notation décimale : Masque de sous-réseau. . . . . . . . . : 255.255.240.0
```

### ☀️ Déso pas déso

```
- Adresse de réseau : 10.33.64.0
- Adresse de broadcast : 10.33.79.255
- Nombre d'adresses IP disponibles : 4096
```

### ☀️ Hostname

```
PS C:\Windows\system32> hostname
LAPTOP-QNFQ1IVC
```

### ☀️ Passerelle du réseau

```
PS C:\Windows\system32> ipconfig /all

Carte réseau sans fil Wifi Ynov :
Passerelle par défaut. . . . . . . . . : 10.33.79.254
```

```
PS C:\Windows\system32> arp -a
10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
```

### ☀️ Serveur DHCP et DNS

```
PS C:\Windows\system32> ipconfig /all

Carte réseau sans fil Wifi Ynov :
Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       1.1.1.1
```

### ☀️ Table de routage

```
PS C:\Windows\system32> route print -4

IPv4 Table de routage
===========================================================================
Itinéraires actifs :
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.170     30
```

---

## II. Go further

### ☀️ Hosts ?

```
PS C:\Windows\system32\drivers\etc> cat .\hosts

1.1.1.1 b2.hello.vous
```

```
PS C:\Windows\system32\drivers\etc> ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 10ms, Moyenne = 10ms
```


### ☀️ Go mater une vidéo youtube et déterminer, pendant qu'elle tourne...
```
PS C:\Users\bapti> nslookup youtube.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    youtube.com
Addresses:  2a00:1450:4007:80c::200e
          172.217.20.174
```

```
PS C:\Users\bapti> netstat -n

Connexions actives
TCP    10.33.76.170:62527     172.217.20.174:443     TIME_WAIT
```

### ☀️ Requêtes DNS

```
PS C:\Users\bapti> nslookup 174.43.238.89
>>
Serveur :   dns.google
Address:  8.8.8.8

Nom :    89.sub-174-43-238.myvzw.com
Address:  174.43.238.89
```

```
PS C:\Users\bapti> nslookup www.ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.ynov.com
Addresses:  2606:4700:20::681a:ae9
          2606:4700:20::681a:be9
          2606:4700:20::ac43:4ae2
          104.26.11.233
          104.26.10.233
          172.67.74.226
```

### ☀️ Hop hop hop

```
PS C:\Users\bapti> tracert www.ynov.com
>>

Détermination de l’itinéraire vers www.ynov.com [104.26.11.233]
avec un maximum de 30 sauts :

  1     4 ms     1 ms     1 ms  10.33.79.254
  2     4 ms     2 ms     2 ms  145.117.7.195.rev.sfr.net [195.7.117.145]
  3     1 ms     1 ms     1 ms  237.195.79.86.rev.sfr.net [86.79.195.237]
  4     2 ms     2 ms     2 ms  196.224.65.86.rev.sfr.net [86.65.224.196]
  5    10 ms    11 ms    10 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  6    11 ms    10 ms     9 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  7    13 ms    14 ms    15 ms  141.101.67.48
  8    11 ms    11 ms    12 ms  172.71.124.4
  9     9 ms     9 ms    12 ms  104.26.11.233
```

### ☀️ IP publique

```
195.7.117.146
```


### ☀️ Scan réseau

Nombre de machine : 43

```
PS C:\Users\bapti> arp -a | Select-String "10.33"

Interface : 10.33.76.170 --- 0x16
  10.33.64.112          b0-a4-60-cf-f4-8b     dynamique
  10.33.64.136          e0-0a-f6-6d-2d-fd     dynamique
  10.33.65.152          a4-c3-f0-b4-b3-06     dynamique
  10.33.65.188          32-fd-2f-fc-81-4a     dynamique
  10.33.65.222          c8-89-f3-e8-db-ef     dynamique
  10.33.65.223          8c-85-90-53-24-b3     dynamique
  10.33.65.226          d0-c6-37-7a-73-b7     dynamique
  10.33.65.236          24-ee-9a-bc-1b-19     dynamique
  10.33.65.240          b0-60-88-51-8d-f8     dynamique
  10.33.66.10           28-11-a8-30-cf-05     dynamique
  10.33.66.47           10-63-c8-76-29-19     dynamique
  10.33.66.172          dc-41-a9-ec-90-aa     dynamique
  10.33.68.35           b0-3c-dc-ae-ab-6e     dynamique
  10.33.68.74           c0-b6-f9-70-dc-08     dynamique
  10.33.68.219          00-93-37-94-13-a8     dynamique
  10.33.69.7            ac-74-b1-41-2e-63     dynamique
  10.33.69.115          a8-a7-95-1f-fe-ad     dynamique
  10.33.69.248          14-5a-fc-64-f0-99     dynamique
  10.33.71.1            84-c5-a6-d6-05-6a     dynamique
  10.33.71.18           48-e7-da-a7-c7-5f     dynamique
  10.33.71.57           04-ea-56-57-ef-4a     dynamique
  10.33.71.99           b0-3c-dc-ae-ab-6e     dynamique
  10.33.71.104          6c-94-66-1f-be-8b     dynamique
  10.33.71.112          b0-3c-dc-ae-ab-6e     dynamique
  10.33.71.115          c4-23-60-eb-cd-5f     dynamique
  10.33.71.123          b0-3c-dc-ae-ab-6e     dynamique
  10.33.71.150          10-a5-1d-85-3a-90     dynamique
  10.33.71.238          60-e9-aa-dd-ee-4d     dynamique
  10.33.73.4            e6-54-24-2e-1b-b4     dynamique
  10.33.73.190          c8-b2-9b-38-2b-88     dynamique
  10.33.73.204          e4-5e-37-3b-02-a9     dynamique
  10.33.74.177          8c-85-90-cf-7d-95     dynamique
  10.33.75.207          f8-ac-65-0f-5b-9f     dynamique
  10.33.76.10           dc-f5-05-ce-ec-f7     dynamique
  10.33.76.181          f8-89-d2-e7-0b-5f     dynamique
  10.33.76.186          30-03-c8-e7-7c-25     dynamique
  10.33.76.190          7c-21-4a-e4-8a-27     dynamique
  10.33.76.222          4c-03-4f-e9-73-f7     dynamique
  10.33.76.225          d8-f3-bc-54-c7-8f     dynamique
  10.33.76.226          48-e7-da-29-d8-b3     dynamique
  10.33.77.98           5c-87-9c-e4-44-2c     dynamique
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
  10.33.79.255          ff-ff-ff-ff-ff-ff     statique
```

---

## III. Le requin

### ☀️ Capture ARP

[Lien vers capture ARP](./arp.pcap)

### ☀️ Capture DNS

[Lien vers capture DNS](./dns.pcap)

### ☀️ Capture TCP

[Lien vers capture TCP](./tcp.pcap)
import asyncio
import websockets

async def handle_client(websocket):
    client = await websocket.recv()
    print(f"{client}")
    greeting = f"Hello client ! Received {client}!"
    await websocket.send(greeting)


async def main():
    async with websockets.serve(handle_client, "10.1.1.10", 13337):
        await asyncio.Future()


if __name__ == "__main__":
    asyncio.run(main())
import asyncio
import redis.asyncio as redis # on utilise la version asynchrone de la lib

async def main():
    # connexion au serveur Redis
    client = redis.Redis(host="10.1.1.12", port=6379)
    
    # on définit la clé 'cat' à la valeur 'meo'
    await client.set('cat', 'meow')
    # on définit la clé 'dog' à la valeur 'waf'
    await client.set('dog', 'waf')

    # récupération de la valeur de 'cat'
    what_the_cat_says = await client.get('cat')

    # ça devrait print 'meow'
    print(what_the_cat_says)

    # on ferme la connexion proprement
    await client.aclose()

if __name__ == "__main__":
    asyncio.run(main())

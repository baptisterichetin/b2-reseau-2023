# III. Serveur Web HTTP

## 1. Serveur Web

### 🌞 tp5_web_serv_1.py un serveur HTTP 

[tp5_web_serv_1.py](/tp5/py/server_web/tp5_web_serv_1.py)

```
[baptiste@bsclient py]$ curl 10.1.1.10:13337
<h1>Hello, je suis un serveur HTTP</h1>[baptiste@bsclient py]
```

## 2. Client Web

### 🌞 tp5_web_client_2.py un client HTTP

[tp5_web_client_2.py](/tp5/py/tp5_web_client_2.py)

```
[baptiste@bsclient py]$ python tp5_web_client_2.py
Status: 200 OK
Headers: [('Server', 'SimpleHTTP/0.6 Python/3.9.16'), ('Date', 'Fri, 01 Dec 2023 16:41:01 GMT'), ('Content-type', 'text/html')]
Response body: <h1>Hello, je suis un serveur HTTP</h1>
```

## 3. Délivrer des pages web

### 🌞 tp5_web_serv_3.py

[tp5_web_serv_3.py](/tp5/py/server_web/tp5_web_serv_3.py)

```
[baptiste@bsserver server_web]$ python tp5_web_serv_3.py
Serveur HTTP démarré...
10.1.1.11 - - [01/Dec/2023 18:14:45] "GET /toto.html HTTP/1.1" 200 -
```
```
[baptiste@bsclient py]$ python tp5_web_client_2.py
Status: 200 OK
Headers: [('Server', 'SimpleHTTP/0.6 Python/3.9.16'), ('Date', 'Fri, 01 Dec 2023 17:14:45 GMT'), ('Content-type', 'type/html')]
Response body: <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ma Page Web</title>
</head>
<body>

    <h1>Bienvenue sur ma page web !</h1>
    <p>C'est une page HTML de base.</p>

</body>
</html>
```

## 4. Quelques logs

### 🌞 tp5_web_serv_4.py

[tp5_web_serv_4.py](/tp5/py/server_web/tp5_web_serv_4.py)

## 5. File download

### 🌞 tp5_web_serv_5.py

[tp5_web_serv_5.py](/tp5/py/server_web/tp5_web_serv_5.py)




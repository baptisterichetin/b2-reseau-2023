import http.server
import socketserver

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        # Obtenez le chemin du fichier demandé par le client
        file_path = self.translate_path(self.path)

        try:
            # Ouvrez le fichier en mode binaire
            with open(file_path, "rb") as file:
                # Lisez le contenu du fichier
                file_content = file.read()

                # Envoyez la réponse HTTP avec le contenu du fichier
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(file_content)
        except FileNotFoundError:
            # Si le fichier n'est pas trouvé, renvoyez une erreur 404
            self.send_error(404, "File Not Found")

if __name__ == "__main__":
    # Créez le serveur HTTP avec votre gestionnaire personnalisé
    with socketserver.TCPServer(('10.1.1.10', 13337), MyHandler) as httpd:
        print('Serveur HTTP démarré...')
        httpd.serve_forever()

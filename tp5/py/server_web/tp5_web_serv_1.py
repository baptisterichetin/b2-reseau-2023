import http.server
import socketserver

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):

        self.send_response(200)

        self.send_header('Content-type', 'text/html')
        self.end_headers()

        self.wfile.write(b'<h1>Hello, je suis un serveur HTTP</h1>')


with socketserver.TCPServer(('10.1.1.10', 13337), MyHandler) as httpd:
    print('Serveur HTTP démarré...')
    httpd.serve_forever()
import http.server
import socketserver
import os

class MyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        file_path = self.translate_path(self.path)

        try:
            with open(file_path, "rb") as file:
                file_size = os.path.getsize(file_path)

                self.send_response(200)
                self.send_header('Content-type', 'application/octet-stream')
                self.send_header('Content-Length', str(file_size))
                self.end_headers()

                chunk_size = 1024
                while True:
                    chunk = file.read(chunk_size)
                    if not chunk:
                        break
                    self.wfile.write(chunk)
        except FileNotFoundError:
            self.send_error(404, "File Not Found")


with socketserver.TCPServer(('10.1.1.10', 13337), MyHandler) as httpd:
    print('\033[94mServeur HTTP démarré...\033[0m')
    httpd.serve_forever()

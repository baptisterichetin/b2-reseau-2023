import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('10.1.1.10', 13337))

s.listen(1)
conn, addr = s.accept()
print(f"Connexion établie avec {addr[0]}")

while True:

    try:
        header = conn.recv(4)
        if not header: break

        msg_length = int.from_bytes(header[0:4], byteorder='big')

        print(f"Taille du message : {msg_length}")

        chunks = []

        bytes_recd = 0

        while bytes_recd < msg_length:
            chunks.append(conn.recv(min(msg_length - bytes_recd, 1024)))

            if not chunks:
                raise RuntimeError("Socket connection broken")

            bytes_recd += len(chunks[-1])

        message_received = b''.join(chunks).decode('utf-8')
        print(f"Message recu : {message_received}")

        res  = eval(message_received)
        conn.send(str(res).encode())

    except socket.error:
        print("Error Occured.")
        break

conn.close()
s.close()
import socket
import os

host = '10.1.1.10'
port = 13337

def checkUserInput(userInput) -> bool:
    if '+' not in userInput and '-' not in userInput and '*' not in userInput:
        return False

    splitUserInput = userInput.split(' ')
    if len(splitUserInput) < 3:
        return False
    try:
        int(splitUserInput[0])
        int(splitUserInput[2])
    except ValueError:
        return False

    if int(splitUserInput[0]) > 4294967295 or int(splitUserInput[2]) > 4294967295:
        return False

def checkLength(userInput) -> int:
    splitUserInput = userInput.split(' ')
    return len(splitUserInput)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect((host, port))
except:
    print(f"\033[31;1mERROR Impossible de se connecter au serveur {host} sur le port {port}\033[0m")
    exit(1)

while True:
    try:
        print("Entrez votre calcul :")
        data = input(">>> ")

        if checkUserInput(data) == False:
            print("\033[31;1mERROR Mauvais format de calcul. Veuillez respecter le format suivant : <nombre> <opérateur> <nombre>\033[0m")
            continue

        encoded_msg = data.encode('utf-8')

        msg_length = len(encoded_msg)

     

        header = msg_length.to_bytes(4, byteorder='big')

        payload = header + encoded_msg + b'<clafin>'

        print(payload)
        s.sendall(payload)

        data = s.recv(1024)

        print(f"Le serveur a répondu : {repr(data.decode('utf-8'))}")

        if not data:
            break

    except KeyboardInterrupt:
        print("\n\033[31;1mERROR Interruption par l'utilisateur.\033[0m")
        s.close()
        exit(1)

s.close()
exit(0)
